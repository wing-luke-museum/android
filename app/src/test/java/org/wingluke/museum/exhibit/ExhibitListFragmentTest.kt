package org.wingluke.museum.exhibit

import android.widget.TextView
import androidx.core.view.get
import androidx.core.view.isVisible
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.test.ext.junit.runners.AndroidJUnit4
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.unmockkAll
import io.mockk.verify
import kotlinx.coroutines.flow.flow
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.wingluke.museum.R
import org.wingluke.museum.annotation.ApplicationScope
import org.wingluke.museum.mvi.ViewState
import org.wingluke.museum.mvi.ViewState.Data
import org.wingluke.museum.mvi.ViewState.Empty
import org.wingluke.museum.room.Exhibit
import org.wingluke.museum.ui.exhibit.ExhibitListFragment
import org.wingluke.museum.ui.exhibit.ExhibitsViewModel
import org.wingluke.museum.ui.exhibit.ExhibitsViewState
import toothpick.testing.ToothPickRule

private const val TITLE = "title"
private const val DESCRIPTION = "description"

@RunWith(AndroidJUnit4::class)
class ExhibitListFragmentTest {

    @get:Rule
    var toothPickRule: ToothPickRule = ToothPickRule(this)

    private val exhibitsViewModel = mockk<ExhibitsViewModel>()
    private val navController = mockk<NavController>()

    private val scenario by lazy {
        launchFragmentInContainer<ExhibitListFragment>(themeResId = R.style.Theme_MaterialComponents)
    }

    @Before
    fun setUp() {
        toothPickRule.testModule.bind(ExhibitsViewModel::class.java).toInstance(exhibitsViewModel)
        toothPickRule.setScopeName(ApplicationScope::class.java)

        every { exhibitsViewModel.flow } returns flow { emit(Empty<ExhibitsViewState>()) }
        every { exhibitsViewModel.load() } returns Unit

        mockkStatic(NavHostFragment::class)
        every { NavHostFragment.findNavController(any()) } returns navController
    }

    @After
    fun tearDown() = unmockkAll()

    @Test
    fun `Test Error state`() {
        every { exhibitsViewModel.flow } returns flow { emit(ViewState.Error<ExhibitsViewState>(Throwable("error"))) }

        scenario.onFragment { assertTrue(it.binding.view.errorMessage.isVisible) }
    }

    @Test
    fun `Test empty state`() {
        every { exhibitsViewModel.flow } returns flow { emit(Data(ExhibitsViewState(listOf()))) }

        scenario.onFragment { assertTrue(it.binding.view.errorMessage.isVisible) }
    }

    @Test
    fun `Test Non Empty List state`() {
        val exhibit = Exhibit(id = "id", title = TITLE, description = DESCRIPTION)
        every { exhibitsViewModel.flow } returns flow { emit(Data(ExhibitsViewState(listOf(exhibit)))) }

        scenario.onFragment { fragment ->
            assertEquals(2, fragment.binding.view.exhibits.adapter?.itemCount)
            assertTrue(fragment.binding.view.exhibits.isVisible)
            with(fragment.binding.view.exhibits[1]) {
                assertEquals(TITLE, findViewById<TextView>(R.id.title).text)
                assertEquals(DESCRIPTION, findViewById<TextView>(R.id.description).text)
            }
        }
    }

    @Test
    fun `Test load called on empty`() {
        every { exhibitsViewModel.load() } returns Unit

        scenario.onFragment { verify(exactly = 1) { exhibitsViewModel.load() } }
    }
}
