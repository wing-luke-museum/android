package org.wingluke.museum.exhibit

import androidx.test.ext.junit.runners.AndroidJUnit4
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.flow.flow
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.wingluke.museum.mvi.ViewState
import org.wingluke.museum.room.Exhibit
import org.wingluke.museum.rules.CoroutinesTestRule
import org.wingluke.museum.test
import org.wingluke.museum.ui.exhibit.ExhibitRepository
import org.wingluke.museum.ui.exhibit.ExhibitsViewModel

@RunWith(AndroidJUnit4::class)
class ExhibitsViewModelTest {
    @get:Rule
    val coroutinesTestRule = CoroutinesTestRule()
    private val exhibitRepository = mockk<ExhibitRepository>()
    private val viewModel: ExhibitsViewModel =
        ExhibitsViewModel(exhibitRepository, coroutinesTestRule.dispatcherProvider)

    @Test
    fun `Test Load Intent`() = coroutinesTestRule.test {
        val collector = viewModel.flow.test(this)
        every { exhibitRepository.exhibits } returns flow { emit(listOf<Exhibit>()) }

        viewModel.load()
        collector
            .assertValueAt(0) { it is ViewState.Empty }
            .assertValueAt(1) { it is ViewState.Data }
            .dispose()
    }

    @Test
    fun `Test Empty Initial State`() = coroutinesTestRule.test {
        viewModel.flow.test(this).assertValueAt(0) { it is ViewState.Empty }.dispose()
    }
}
