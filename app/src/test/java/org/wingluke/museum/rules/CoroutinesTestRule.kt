package org.wingluke.museum.rules

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.rules.TestWatcher
import org.junit.runner.Description
import org.wingluke.museum.mvi.DispatcherProvider
import kotlin.coroutines.CoroutineContext

class CoroutinesTestRule(
    val dispatcher: TestCoroutineDispatcher = TestCoroutineDispatcher()
) : TestWatcher(), CoroutineContext by dispatcher {

    val dispatcherProvider = object : DispatcherProvider {
        override val default = dispatcher
        override val main = dispatcher
        override val io = dispatcher
    }

    override fun starting(description: Description) {
        super.starting(description)
        Dispatchers.setMain(dispatcher)
    }

    override fun finished(description: Description) {
        super.finished(description)
        Dispatchers.resetMain()
        dispatcher.cleanupTestCoroutines()
    }

    fun test(block: suspend TestCoroutineScope.() -> Unit): Unit = dispatcher.runBlockingTest(block)
}
