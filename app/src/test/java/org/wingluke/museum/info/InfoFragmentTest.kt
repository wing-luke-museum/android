package org.wingluke.museum.info

import androidx.core.view.isVisible
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.ext.junit.runners.AndroidJUnit4
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.flow.flow
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.wingluke.museum.annotation.ApplicationScope
import org.wingluke.museum.mvi.ViewState
import org.wingluke.museum.ui.info.HourViewModel
import org.wingluke.museum.ui.info.InfoFragment
import toothpick.testing.ToothPickRule

@RunWith(AndroidJUnit4::class)
class InfoFragmentTest {

    @get:Rule
    var toothPickRule: ToothPickRule = ToothPickRule(this)

    private val hourViewModel = mockk<HourViewModel> {
        every { flow } returns flow { emit(ViewState.Data("Hours")) }
    }

    private val scenario by lazy { launchFragmentInContainer<InfoFragment>() }

    @Before
    fun before() {
        toothPickRule.testModule.bind(HourViewModel::class.java).toInstance(hourViewModel)
        toothPickRule.setScopeName(ApplicationScope::class.java)
    }

    @Test
    fun `Test Happy Path`() {
        scenario.onFragment { fragment ->
            with(fragment.binding.view) {
                assertTrue(title.isVisible)
                assertTrue(hoursTitle.isVisible)
                assertTrue(call.isVisible)
                assertTrue(memberships.isVisible)
                assertTrue(hours.text.isNotBlank())
            }
        }
    }
}
