package org.wingluke.museum.ui.map

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils.loadAnimation
import androidx.annotation.VisibleForTesting
import androidx.core.view.isVisible
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_EXPANDED
import com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_HIDDEN
import com.google.android.material.bottomsheet.BottomSheetBehavior.from
import org.wingluke.museum.R
import org.wingluke.museum.databinding.FragmentMapBinding
import org.wingluke.museum.extentions.viewBinding
import org.wingluke.museum.imdf.Archive
import org.wingluke.museum.imdf.Level
import org.wingluke.museum.ui.InjectionFragment
import org.wingluke.museum.ui.map.units.UnitMarkerOptions

private fun bottomSheetCallback(
    onStateChanged: (bottomSheet: View, newState: Int) -> Unit = { _, _ -> },
    onSlide: (bottomSheet: View, slideOffset: Float) -> Unit = { _, _ -> }
) = object : BottomSheetBehavior.BottomSheetCallback() {
    override fun onStateChanged(bottomSheet: View, newState: Int) = onStateChanged.invoke(bottomSheet, newState)
    override fun onSlide(bottomSheet: View, slideOffset: Float) = onSlide.invoke(bottomSheet, slideOffset)
}

class MapFragment : InjectionFragment(), OnMapReadyCallback, LevelPicker.OnLevelChangeCallback {
    @VisibleForTesting
    val binding = viewBinding(FragmentMapBinding::inflate)

    private var googleMap: GoogleMap? = null

    private val bottomSheetBehavior by lazy { from(binding.view.legend.root) }

    private val fadeIn by lazy { loadAnimation(requireContext(), R.anim.map_overlay_fade_in) }
    private val fadeOut by lazy { loadAnimation(requireContext(), R.anim.map_overlay_fade_out) }

    private fun animationListener(
        onAnimationStart: (Animation?) -> Unit = {},
        onAnimationEnd: (Animation?) -> Unit = {},
        onAnimationRepeat: (Animation?) -> Unit = {}
    ) = object : Animation.AnimationListener {
        override fun onAnimationStart(animation: Animation?) = onAnimationStart.invoke(animation)
        override fun onAnimationEnd(animation: Animation?) = onAnimationEnd.invoke(animation)
        override fun onAnimationRepeat(animation: Animation?) = onAnimationRepeat.invoke(animation)
    }

    private fun toggleLegend() = if (bottomSheetBehavior.state == STATE_EXPANDED) hideLegend() else showLegend()
    private fun showLegend() = with(binding.view) {
        mapOverlay.startAnimation(fadeIn)
        bottomSheetBehavior.state = STATE_EXPANDED
    }

    private fun hideLegend() = with(binding.view) {
        if (bottomSheetBehavior.state == STATE_HIDDEN) return@with

        mapOverlay.startAnimation(fadeOut)
        bottomSheetBehavior.state = STATE_HIDDEN
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        binding.inflate(inflater, container).root.also {
            (childFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment)?.getMapAsync(this)
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bottomSheetBehavior.isHideable = true
        bottomSheetBehavior.state = STATE_HIDDEN
        bottomSheetBehavior.addBottomSheetCallback(
            bottomSheetCallback(onStateChanged = { _, newState -> when (newState) { STATE_HIDDEN -> hideLegend() } })
        )

        with(binding.view) {
            fadeIn.setAnimationListener(animationListener(onAnimationStart = { mapOverlay.isVisible = true }))
            fadeOut.setAnimationListener(animationListener(onAnimationEnd = { mapOverlay.isVisible = false }))

            mapOverlay.isVisible = false
            floorPicker.root.callback = this@MapFragment
            legend.header.setOnClickListener { hideLegend() }
            floorPicker.info.setOnClickListener { toggleLegend() }
        }
    }

    override fun onMapReady(map: GoogleMap) = with(Archive(requireContext().assets)) {
        googleMap = map.apply {
            moveCamera(CameraUpdateFactory.newCameraPosition(MuseumCameraPositionBuilder.build()))
            setMapStyle(MapStyleOptions.loadRawResourceStyle(requireContext(), R.raw.map_style))
            addPolygon(VenuePolygonOptions(venue, requireContext()))

            setOnMapClickListener { hideLegend() }
        }

        binding.view.floorPicker.root.levels = levels
    }

    override fun setLevel(level: Level) {
        val archive = Archive(requireContext().assets)
        val levelUnits = archive.units.filter { it.properties.levelID == level.identifier }

        googleMap?.run {
            clear()
            addPolygon(VenuePolygonOptions(archive.venue, requireContext()))
            levelUnits.forEach { levelUnit ->
                addPolygon(UnitPolygonOptions(levelUnit, requireContext()))
                UnitMarkerOptions(levelUnit)?.let { options -> addMarker(options) }
            }

            archive.amenities.filter { amenity -> amenity.unitIDs.any { unitID -> levelUnits.any { unit -> unit.identifier == unitID } } }.forEach {
                AmenityMarkerOptions(it, requireContext())?.let { options -> addMarker(options) }
            }
        }
    }
}
