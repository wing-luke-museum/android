package org.wingluke.museum.ui.exhibit

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map
import kotlinx.serialization.ExperimentalSerializationApi
import org.wingluke.museum.mvi.DispatcherProvider
import org.wingluke.museum.mvi.FlowStateViewModel
import org.wingluke.museum.mvi.ViewState
import org.wingluke.museum.room.Exhibit
import org.wingluke.museum.ui.exhibit.ExhibitsViewModel.ExhibitsIntent
import toothpick.InjectConstructor

data class ExhibitsViewState(val exhibits: List<Exhibit> = listOf())

@InjectConstructor
class ExhibitsViewModel(
    private val exhibitRepository: ExhibitRepository,
    dispatcherProvider: DispatcherProvider
) : FlowStateViewModel<ExhibitsIntent, ExhibitsViewState>(dispatcherProvider.io) {
    fun load() = doAction(ExhibitsIntent.Load)

    sealed class ExhibitsIntent {
        object Load : ExhibitsIntent()
    }

    @ExperimentalSerializationApi
    @ExperimentalCoroutinesApi
    override fun transformFlow(intentStream: Flow<ExhibitsIntent>): Flow<ViewState<ExhibitsViewState>> =
        intentStream
            .flatMapLatest { intent ->
                when (intent) {
                    is ExhibitsIntent.Load -> exhibitRepository.exhibits.map { ExhibitsViewState(it) }
                }
            }.toViewState()
}
