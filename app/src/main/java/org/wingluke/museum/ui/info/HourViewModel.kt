package org.wingluke.museum.ui.info

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.serialization.ExperimentalSerializationApi
import org.wingluke.museum.mvi.DispatcherProvider
import org.wingluke.museum.mvi.FlowStateViewModel
import org.wingluke.museum.mvi.ViewState
import org.wingluke.museum.service.WingLukeClient
import org.wingluke.museum.ui.info.HourViewModel.HourIntent
import toothpick.InjectConstructor

@InjectConstructor
class HourViewModel(
    private val client: WingLukeClient,
    dispatcherProvider: DispatcherProvider
) : FlowStateViewModel<HourIntent, String>(dispatcherProvider.io) {
    fun load() = doAction(HourIntent.Load)

    sealed class HourIntent {
        object Load : HourIntent()
    }

    @ExperimentalSerializationApi
    @ExperimentalCoroutinesApi
    override fun transformFlow(intentStream: Flow<HourIntent>): Flow<ViewState<String>> =
        intentStream
            .flatMapLatest { intent ->
                when (intent) {
                    is HourIntent.Load -> flow { emit(client.fetchHours()) }
                }
            }.toViewState()
}
