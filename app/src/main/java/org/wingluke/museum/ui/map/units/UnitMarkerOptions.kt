package org.wingluke.museum.ui.map.units

import android.graphics.Bitmap
import androidx.core.graphics.applyCanvas
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.MarkerOptions
import org.wingluke.museum.imdf.Unit

object UnitMarkerOptions {
    operator fun invoke(unit: Unit): MarkerOptions? {
        if (unit.name.isNullOrEmpty()) { return null }
        return MarkerOptions()
            .position(unit.geometry.center)
            .anchor(centerCoordinate, centerCoordinate)
            .icon(BitmapDescriptorFactory.fromBitmap(
                nameBitmap(unit)))
    }

    private fun nameBitmap(unit: Unit): Bitmap {
        val layout = TextLayout(unit, StrokeTextPaint())

        val bitmap = Bitmap.createBitmap(layout.width, layout.height, Bitmap.Config.ARGB_8888)
            .applyCanvas {
                // draw the layout twice to fake having outlined text
                layout.draw(this)
                TextLayout(unit, FillTextPaint()).draw(this)
            }

        return bitmap
    }

    private const val centerCoordinate = 0.5f
}
