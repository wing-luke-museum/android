package org.wingluke.museum.ui.map

import com.google.android.gms.maps.model.PolygonOptions
import org.wingluke.museum.imdf.Coordinate
import org.wingluke.museum.imdf.Polygon

object PolygonOptionsFactory {
    fun fromPolygon(polygon: Polygon): PolygonOptions {
        val linearRings = polygon.linearRings
        val firstRing = linearRings.first()
        val holes = linearRings.drop(1)

        val outerPolygon = PolygonOptions()
            .addAll(firstRing.coordinates.map(Coordinate::toLatLng))

        return holes.fold(outerPolygon) { currentPolygon, ring ->
            currentPolygon.addHole(ring.coordinates.map(Coordinate::toLatLng))
        }
    }
}
