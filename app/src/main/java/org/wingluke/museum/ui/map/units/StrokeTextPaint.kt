package org.wingluke.museum.ui.map.units

import android.content.res.Resources
import android.graphics.Color
import android.util.TypedValue

internal class StrokeTextPaint : BaseTextPaint() {
    init {
        color = Color.WHITE
        style = Style.STROKE
        strokeWidth = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            2f, Resources.getSystem().displayMetrics)
    }
}
