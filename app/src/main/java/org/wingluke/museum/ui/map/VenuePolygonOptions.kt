package org.wingluke.museum.ui.map

import android.content.Context
import androidx.core.content.res.ResourcesCompat
import com.google.android.gms.maps.model.PolygonOptions
import org.wingluke.museum.R
import org.wingluke.museum.imdf.Venue

object VenuePolygonOptions {
    operator fun invoke(venue: Venue, context: Context): PolygonOptions {
        val fillColor = ResourcesCompat.getColor(context.resources, R.color.map_venue, null)
        return PolygonOptionsFactory.fromPolygon(venue.polygon)
            .fillColor(fillColor)
    }
}
