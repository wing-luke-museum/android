package org.wingluke.museum.ui.map.units

import android.graphics.Color

internal class FillTextPaint : BaseTextPaint() {
    init {
        color = Color.BLACK
        style = Style.FILL
    }
}
