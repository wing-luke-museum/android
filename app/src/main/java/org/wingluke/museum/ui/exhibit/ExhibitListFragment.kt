package org.wingluke.museum.ui.exhibit

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.VisibleForTesting
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import org.wingluke.museum.R
import org.wingluke.museum.databinding.FragmentExhibitsListBinding
import org.wingluke.museum.extentions.viewBinding
import org.wingluke.museum.flow.collectIn
import org.wingluke.museum.mvi.ViewState.Data
import org.wingluke.museum.mvi.ViewState.Empty
import org.wingluke.museum.mvi.ViewState.Error
import org.wingluke.museum.room.Exhibit
import org.wingluke.museum.ui.InjectionFragment
import toothpick.ktp.delegate.inject

class ExhibitListFragment : InjectionFragment() {
    @VisibleForTesting
    val binding = viewBinding(FragmentExhibitsListBinding::inflate)

    private val exhibitsViewModel: ExhibitsViewModel by inject()

    private val exhibitsHeaderAdapter by lazy { ExhibitHeaderAdapter() }
    private val exhibitsItemsAdapter by lazy { ExhibitAdapter(requireContext()) }
    private val exhibitsAdapter by lazy { ConcatAdapter(exhibitsHeaderAdapter, exhibitsItemsAdapter) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        binding.inflate(inflater, container).root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) =
        super.onViewCreated(view, savedInstanceState).also { bind() }

    private fun bind() = with(binding.view) {
        bindExhibits()
        bindSwipeRefresh()
    }

    private fun bindExhibits() = with(binding.view.exhibits) {
        adapter = exhibitsAdapter
        layoutManager = LinearLayoutManager(context)
    }

    private fun bindSwipeRefresh() = with(binding.view.swipeRefresh) {
        setOnRefreshListener {
        isRefreshing = true
        exhibitsViewModel.load()
    }
    }

    override fun onStart() {
        super.onStart()

        exhibitsViewModel.flow.collectIn(viewLifecycleOwner.lifecycleScope) { viewState ->
            when (viewState) {
                is Data -> onData(viewState.data.exhibits)
                is Empty -> exhibitsViewModel.load()
                is Error -> showEmpty()
            }

            binding.view.swipeRefresh.isRefreshing = false
        }
    }

    private fun onData(exhibits: List<Exhibit>) {
        exhibitsHeaderAdapter.submitList()
        exhibitsItemsAdapter.submitList(exhibits.toMutableList())

        if (exhibits.isEmpty()) showEmpty() else showList()
    }

    private fun showList() = toggleContentVisibility()

    private fun toggleContentVisibility(isVisible: Boolean = true) = with(binding.view) {
        exhibits.isVisible = isVisible
        errorMessage.isVisible = !isVisible
    }

    private fun showEmpty() = showError(getString(R.string.exhibits_empty))

    private fun showError(message: String) {
        toggleContentVisibility(false)
        binding.view.errorMessage.text = message
    }
}
