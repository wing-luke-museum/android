package org.wingluke.museum.ui.map

import android.content.Context
import android.graphics.Bitmap
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.graphics.applyCanvas
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.MarkerOptions
import org.wingluke.museum.R
import org.wingluke.museum.imdf.Amenity

object AmenityMarkerOptions {
    operator fun invoke(amenity: Amenity, context: Context): MarkerOptions? {
        val icon = icon(amenity, context) ?: return null
        return MarkerOptions()
            .position(amenity.geometry.toLatLng())
            .anchor(centerCoordinate, centerCoordinate)
            .icon(BitmapDescriptorFactory.fromBitmap(icon))
    }

    private fun iconResource(amenity: Amenity): Int? = when (amenity.category) {
        Amenity.Category.Elevator -> R.drawable.ic_elevator
        Amenity.Category.DrinkingFountain -> R.drawable.ic_fountain
        Amenity.Category.Stairs -> R.drawable.ic_stairs
        Amenity.Category.Information -> R.drawable.ic_visitor_services
        else -> null
    }

    private fun icon(amenity: Amenity, context: Context): Bitmap? {
        val iconResource = iconResource(amenity) ?: return null
        val iconDrawable = AppCompatResources.getDrawable(context, iconResource) ?: return null

        return Bitmap.createBitmap(iconDrawable.intrinsicWidth, iconDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
            .applyCanvas {
                iconDrawable.setBounds(0, 0, iconDrawable.intrinsicWidth, iconDrawable.intrinsicHeight)
                iconDrawable.draw(this)
            }
    }

    private const val centerCoordinate = 0.5f
}
