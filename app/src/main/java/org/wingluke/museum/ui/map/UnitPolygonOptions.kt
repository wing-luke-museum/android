package org.wingluke.museum.ui.map

import android.content.Context
import com.google.android.gms.maps.model.PolygonOptions
import org.wingluke.museum.R
import org.wingluke.museum.imdf.Unit

object UnitPolygonOptions {
    operator fun invoke(unit: Unit, context: Context): PolygonOptions = PolygonOptionsFactory
        .fromPolygon(unit.geometry.polygon)
        .strokeColor(strokeColor(unit, context))
        .strokeWidth(2f)
        .fillColor(fillColor(unit, context))

    private fun fillColor(unit: Unit, context: Context): Int {
        val fillColorResource = when {
            unit.space == "historic" -> R.color.map_unit_historic_fill
            unit.space == "museum" -> R.color.map_unit_museum_fill
            unit.space == "resource" -> R.color.map_unit_resource_fill
            unit.isRestroom -> R.color.map_unit_restroom_fill
            unit.category == Unit.Category.Nonpublic -> R.color.map_unit_nonpublic_fill
            unit.category == Unit.Category.Elevator -> R.color.map_unit_elevator_fill
            else -> R.color.map_unit_other
        }
        return context.resources.getColor(fillColorResource, null)
    }

    private fun strokeColor(unit: Unit, context: Context): Int {
        val strokeColorResource = when {
            unit.space == "historic" -> R.color.map_unit_historic_stroke
            unit.space == "museum" -> R.color.map_unit_museum_stroke
            unit.space == "resource" -> R.color.map_unit_resource_stroke
            unit.isRestroom -> R.color.map_unit_restroom_stroke
            unit.category == Unit.Category.Nonpublic -> R.color.map_unit_nonpublic_stroke
            unit.category == Unit.Category.Elevator -> R.color.map_unit_elevator_stroke
            else -> R.color.map_venue
        }
        return context.resources.getColor(strokeColorResource, null)
    }
}
