package org.wingluke.museum.ui.map

import com.google.android.gms.maps.model.CameraPosition
import org.wingluke.museum.Constants

object MuseumCameraPositionBuilder {
    @Suppress("MagicNumber")
    fun build(): CameraPosition = CameraPosition.Builder()
        .target(Constants.museumLatLng)
        .bearing(180f)
        .zoom(20f)
        .build()
}
