package org.wingluke.museum.ui.exhibit

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.GradientDrawable.Orientation
import androidx.core.content.ContextCompat.getDrawable
import androidx.core.graphics.ColorUtils.calculateLuminance
import androidx.palette.graphics.Palette
import androidx.recyclerview.widget.RecyclerView
import coil.load
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import org.wingluke.museum.R
import org.wingluke.museum.databinding.ExhibitItemBinding
import org.wingluke.museum.room.Exhibit

private const val UNKNOWN_DATE = "???"

internal val minDate = Date(0)
internal val Date?.isMinDate get() = this == minDate

class ExhibitsViewHolder(
    private val exhibitItemBinding: ExhibitItemBinding
) : RecyclerView.ViewHolder(exhibitItemBinding.root) {
    private val simpleDateFormat = SimpleDateFormat("MMM d yyyy", Locale.ENGLISH)

    @Suppress("MagicNumber")
    private val Int.isDark
        get() = calculateLuminance(this) < 0.5

    fun bind(context: Context, exhibit: Exhibit) = with(exhibitItemBinding) {
        title.text = exhibit.title
        description.text = exhibit.description ?: exhibit.subtitle

        date.text = when {
            exhibit.endDate == null || exhibit.endDate.isMinDate -> context.getString(R.string.exhibit_duration_ongoing)
            else -> context.getString(
                R.string.exhibit_date,
                exhibit.startDate?.let(simpleDateFormat::format) ?: UNKNOWN_DATE,
                simpleDateFormat.format(exhibit.endDate)
            )
        }

        exhibit.image?.let { url ->
            background.load(url.toString()) {
                crossfade(true)
                allowHardware(false)
                target(
                    onError = {},
                    onStart = { getDrawable(context, R.drawable.exhibits_placeholder)?.let { setBackgroundAndGradient(it) } },
                    onSuccess = { setBackgroundAndGradient(it) }
                )
            }
        }
    }

    private fun ExhibitItemBinding.setBackgroundAndGradient(drawable: Drawable) {
        background.setImageDrawable(drawable)

        (drawable as? BitmapDrawable)?.bitmap?.let { Palette.from(it).generate() }?.dominantSwatch?.rgb?.let { rgb ->
            val dominantColor = ColorDrawable(rgb)
            val textColor = if (rgb.isDark) Color.WHITE else Color.BLACK

            gradient.background = GradientDrawable(Orientation.BOTTOM_TOP, intArrayOf(rgb, Color.TRANSPARENT))

            title.background = dominantColor
            title.setTextColor(textColor)

            date.background = dominantColor
            date.setTextColor(textColor)

            description.background = dominantColor
            description.setTextColor(textColor)
        }
    }
}
