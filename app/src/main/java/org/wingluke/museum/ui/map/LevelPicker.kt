package org.wingluke.museum.ui.map

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.cardview.widget.CardView
import com.google.android.material.button.MaterialButton
import com.google.android.material.button.MaterialButtonToggleGroup
import org.wingluke.museum.R
import org.wingluke.museum.imdf.Level

class LevelPicker(context: Context, attrs: AttributeSet) : CardView(context, attrs) {
    @Suppress("LateinitUsage")
    private lateinit var levelButtonsGroup: MaterialButtonToggleGroup
    var callback: OnLevelChangeCallback? = null
    var levels: List<Level> = emptyList()
        set(newLevels) {
            field = newLevels
            updateLevelButtons()
        }

    private fun updateLevelButtons() {
        // create the new level buttons
        val newButtons = levels.map { level ->
            val button = LayoutInflater.from(context).inflate(R.layout.map_level_button, null) as MaterialButton
            button.setOnClickListener { callback?.setLevel(level) }
            button.text = level.shortName
            button.id = level.ordinal
            button
        }

        // replace the contents of the toggle group with the new buttons
        levelButtonsGroup.removeAllViews()
        newButtons.forEach { levelButtonsGroup.addView(it) }

        // select first level
        val firstLevel = levels.first()
        levelButtonsGroup.check(firstLevel.ordinal)
        callback?.setLevel(firstLevel)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        levelButtonsGroup = findViewById(R.id.level_buttons)
    }

    interface OnLevelChangeCallback {
        fun setLevel(level: Level)
    }
}
