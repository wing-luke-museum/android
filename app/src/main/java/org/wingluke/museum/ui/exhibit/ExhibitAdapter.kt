package org.wingluke.museum.ui.exhibit

import android.content.Context
import android.view.LayoutInflater.from
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil.ItemCallback
import androidx.recyclerview.widget.ListAdapter
import org.wingluke.museum.databinding.ExhibitItemBinding.inflate
import org.wingluke.museum.room.Exhibit

private class DiffCallback : ItemCallback<Exhibit>() {
    override fun areItemsTheSame(oldItem: Exhibit, newItem: Exhibit): Boolean = oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Exhibit, newItem: Exhibit): Boolean = oldItem == newItem
}

class ExhibitAdapter(private val context: Context) : ListAdapter<Exhibit, ExhibitsViewHolder>(DiffCallback()) {
    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ExhibitsViewHolder(inflate(from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ExhibitsViewHolder, position: Int) {
        holder.bind(context, getItem(position))
    }

    private fun List<Exhibit>.sort() = sortedWith { exhibitA, exhibitB ->
        when {
            exhibitA.endDate.isMinDate && !exhibitB.endDate.isMinDate ->
                if (exhibitA.endDate ?: minDate > exhibitB.endDate) 1 else -1
            exhibitA.startDate.isMinDate && !exhibitB.startDate.isMinDate ->
                if (exhibitA.startDate ?: minDate > exhibitB.startDate) 1 else -1
            else -> 0
        }
    }

    override fun submitList(list: MutableList<Exhibit>?) = submitList(list, null)
    override fun submitList(list: MutableList<Exhibit>?, commitCallback: Runnable?) = super.submitList(list?.sort()?.reversed(), commitCallback)
}
