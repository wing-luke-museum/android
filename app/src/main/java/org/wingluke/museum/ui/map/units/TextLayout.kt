package org.wingluke.museum.ui.map.units

import android.content.res.Resources
import android.text.Layout
import android.text.StaticLayout
import android.util.TypedValue
import org.wingluke.museum.imdf.Unit

internal object TextLayout {
    operator fun invoke(unit: Unit, paint: BaseTextPaint): StaticLayout {
        val scaledWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, standardWidth, Resources.getSystem().displayMetrics).toInt()
        val text = unit.name ?: ""
        return StaticLayout.Builder.obtain(text, 0, text.length, paint, scaledWidth)
            .setAlignment(Layout.Alignment.ALIGN_CENTER)
            .build()
    }

    private const val standardWidth = 75f
}
