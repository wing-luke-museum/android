package org.wingluke.museum.ui.exhibit

import android.annotation.SuppressLint
import android.view.LayoutInflater.from
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import org.wingluke.museum.databinding.ExhibitsTitleBinding
import org.wingluke.museum.databinding.ExhibitsTitleBinding.inflate

class ExhibitHeaderAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var headers: List<Int> = emptyList()

    @SuppressLint("NotifyDataSetChanged")
    fun submitList() {
        headers = listOf(1)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ExhibitsHeaderViewHolder(
        inflate(from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) = Unit

    override fun getItemCount(): Int = headers.size

    class ExhibitsHeaderViewHolder(
        exhibitsTitleBinding: ExhibitsTitleBinding
    ) : RecyclerView.ViewHolder(exhibitsTitleBinding.root)
}
