package org.wingluke.museum.ui.error

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.VisibleForTesting
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import org.wingluke.museum.R
import org.wingluke.museum.databinding.FragmentErrorBinding
import org.wingluke.museum.extentions.viewBinding

private const val ARG_ERROR_TEXT = "ARG_ERROR_TEXT"

fun Fragment.navigateError(errorText: String = "") = findNavController().navigateError(errorText)

fun NavController.navigateError(errorText: String = "") = navigate(R.id.action_error, newErrorFragmentArgs(errorText))

@VisibleForTesting
fun newErrorFragmentArgs(
    errorText: String = ""
) = bundleOf(
    ARG_ERROR_TEXT to errorText
)

class ErrorFragment : Fragment() {
    @VisibleForTesting
    var binding = viewBinding(FragmentErrorBinding::inflate)

    private val errorText: String get() = requireNotNull(arguments?.getString(ARG_ERROR_TEXT))

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        binding.inflate(inflater, container).root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.view.errorMessage.setContent {
            ErrorMessage(errorText.takeUnless { it.isBlank() } ?: getString(R.string.default_error_message))
        }
    }

    @Composable
    private fun ErrorMessage(message: String) {
        Text(text = message)
    }
}
