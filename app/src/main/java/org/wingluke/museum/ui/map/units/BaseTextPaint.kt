package org.wingluke.museum.ui.map.units

import android.content.res.Resources
import android.graphics.Typeface
import android.text.TextPaint
import android.util.TypedValue

internal open class BaseTextPaint : TextPaint() {
    init {
        textSize = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            fontSize, Resources.getSystem().displayMetrics)
        isAntiAlias = true
        typeface = Typeface.DEFAULT_BOLD
    }

    companion object {
        private const val fontSize = 12f
    }
}
