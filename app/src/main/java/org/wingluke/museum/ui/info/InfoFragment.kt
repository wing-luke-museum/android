package org.wingluke.museum.ui.info

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.VisibleForTesting
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import com.flaviofaria.kenburnsview.KenBurnsView
import com.flaviofaria.kenburnsview.Transition
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.MarkerOptions
import org.wingluke.museum.Constants
import org.wingluke.museum.R
import org.wingluke.museum.databinding.FragmentInfoBinding
import org.wingluke.museum.extentions.viewBinding
import org.wingluke.museum.flow.collectIn
import org.wingluke.museum.mvi.ViewState
import org.wingluke.museum.ui.InjectionFragment
import toothpick.ktp.delegate.inject

private const val WING_LUKE_PHONE = "12066235124"
private const val WING_LUKE_MEMBERSHIP_URL = "https://www.wingluke.org/join-give/"
private const val SHARED_PREF_HOUR_KEY = "HOURS"

class InfoFragment : InjectionFragment(), OnMapReadyCallback {
    @VisibleForTesting
    val binding = viewBinding(FragmentInfoBinding::inflate)

    private val hourViewModel: HourViewModel by inject()

    private val dialIntent = Intent(Intent.ACTION_DIAL).apply { data = Uri.parse("tel:$WING_LUKE_PHONE") }
    private val membershipIntent = Intent(Intent.ACTION_VIEW).apply { data = Uri.parse(WING_LUKE_MEMBERSHIP_URL) }
    private val sharedPref by lazy { requireActivity().getPreferences(Context.MODE_PRIVATE) }

    private val heroDrawables by lazy {
        listOf(
            ContextCompat.getDrawable(requireActivity(), R.drawable.info_hero),
            ContextCompat.getDrawable(requireActivity(), R.drawable.info_hero2),
            ContextCompat.getDrawable(requireActivity(), R.drawable.info_hero3),
            ContextCompat.getDrawable(requireActivity(), R.drawable.info_hero4)
        )
    }

    private var transitionCount: Int = 1

    private val heroDrawableIndex
        get() = transitionCount++.also { if (transitionCount >= heroDrawables.size) transitionCount = 0 }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        binding.inflate(inflater, container).root.also {
            val mapFragment = childFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment
            mapFragment?.getMapAsync(this)

            binding.view.call.setOnClickListener { startActivity(dialIntent) }
            binding.view.memberships.setOnClickListener { startActivity(membershipIntent) }

            val transitionListener = transitionListener(onTransitionEnd = {
                binding.view.background.showNext()
                val currentIndex = heroDrawableIndex
                heroDrawables[currentIndex]?.let {
                    if (currentIndex % 2 == 0) {
                        binding.view.img1.setImageDrawable(it)
                    } else binding.view.img2.setImageDrawable(it)
                }
            })

            binding.view.img1.setTransitionListener(transitionListener)
            binding.view.img2.setTransitionListener(transitionListener)
        }

    override fun onStart() {
        super.onStart()

        hourViewModel.flow.collectIn(viewLifecycleOwner.lifecycleScope) {
            when (it) {
                is ViewState.Data -> showHours(it.data)
                is ViewState.Empty -> hourViewModel.load()
                is ViewState.Error -> showPersistedHours()
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        googleMap.apply {
            val markerOptions = MarkerOptions()
                .position(Constants.museumLatLng)
                .title("Wing Luke Museum")

            ResourcesCompat.getDrawable(resources, R.drawable.ic_map_pin, null)
                ?.toBitmap()
                ?.let(BitmapDescriptorFactory::fromBitmap)
                ?.run(markerOptions::icon)

            addMarker(markerOptions)
            moveCamera(CameraUpdateFactory.newLatLng(Constants.museumLatLng))
        }
    }

    private fun transitionListener(
        onTransitionStart: (Transition?) -> Unit = {},
        onTransitionEnd: (Transition?) -> Unit = {},
    ) = object : KenBurnsView.TransitionListener {
        override fun onTransitionStart(transition: Transition?) = onTransitionStart.invoke(transition)
        override fun onTransitionEnd(transition: Transition?) = onTransitionEnd.invoke(transition)
    }

    private fun showHours(hours: String) = with(binding.view) {
        if (hours.isNotEmpty()) {
            this.hours.text = hours
            this.hours.isVisible = true
            hoursTitle.isVisible = true
            persistHours(hours)
        }
    }

    private fun showPersistedHours() = retrieveHours().takeIf { it.isNotEmpty() }?.run(::showHours)

    private fun persistHours(hours: String) {
        sharedPref.edit().apply {
            putString(SHARED_PREF_HOUR_KEY, hours)
            apply()
        }
    }

    private fun retrieveHours(): String = sharedPref.getString(SHARED_PREF_HOUR_KEY, "") ?: ""
}
