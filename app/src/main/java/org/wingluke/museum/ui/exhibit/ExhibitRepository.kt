package org.wingluke.museum.ui.exhibit

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.serialization.ExperimentalSerializationApi
import org.wingluke.museum.room.AppDatabase
import org.wingluke.museum.room.Exhibit
import org.wingluke.museum.service.WingLukeClient
import toothpick.InjectConstructor

@InjectConstructor
class ExhibitRepository(
    private val client: WingLukeClient,
    private val db: AppDatabase,
) {
    private val exhibitDao by lazy { db.exhibitDao() }

    @ExperimentalSerializationApi
    val exhibits: Flow<List<Exhibit>> = exhibitDao.getAll()
        .distinctUntilChanged()
        .mapNotNull {
            if (it.isEmpty()) null.also {
                val remoteExhibits = client.fetchRecent()
                db.exhibitDao().clear()
                db.exhibitDao().insertAll(remoteExhibits)
            } else it
        }
}
