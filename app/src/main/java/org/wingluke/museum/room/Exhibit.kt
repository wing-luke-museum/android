package org.wingluke.museum.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.net.URL
import java.util.Date

@Entity(tableName = "exhibit_table")
data class Exhibit(
    @PrimaryKey val id: String,
    @ColumnInfo(name = "title") val title: String,
    @ColumnInfo(name = "subtitle") val subtitle: String = "",
    @ColumnInfo(name = "duration") val duration: String = "",
    @ColumnInfo(name = "startdate") val startDate: Date? = null,
    @ColumnInfo(name = "enddate") val endDate: Date? = null,
    @ColumnInfo(name = "description") val description: String? = null,
    @ColumnInfo(name = "image") val image: URL? = null,
)
