package org.wingluke.museum.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface ExhibitDao {
    @Query("SELECT * FROM exhibit_table")
    fun getAll(): Flow<List<Exhibit>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(exhibits: List<Exhibit>)

    @Query("DELETE FROM exhibit_table")
    suspend fun clear()
}
