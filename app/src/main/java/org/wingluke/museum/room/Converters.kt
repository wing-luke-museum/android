package org.wingluke.museum.room

import androidx.room.TypeConverter
import java.net.URL
import java.util.Date

class Converters {
    @TypeConverter
    fun toDate(value: Long) = Date(value)

    @TypeConverter
    fun fromDate(date: Date?) = date?.time ?: 0

    @TypeConverter
    fun toUrl(value: String) = URL(value)

    @TypeConverter
    fun fromUrl(url: URL?) = url.toString()
}
