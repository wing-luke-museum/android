package org.wingluke.museum.service

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

fun Overview.parse(): String = en.joinToString("\n") { en ->
    when (en.style) {
        else -> parseChildren(en.children)
    }
}

private fun parseChildren(children: List<Children>): String = children.joinToString("") { child ->
    when (child.type) {
        else -> child.text
    }
}

@Serializable
data class ExhibitResponse(
    val ms: Int? = null,
    val query: String? = null,
    @SerialName("result") val exhibitResult: List<ExhibitResult>
)

@Serializable
data class ExhibitResult(
    @SerialName("_id") val id: String,
    @SerialName("_type") val type: String,
    val bannerURL: String,
    val closingDate: String? = null,
    val openingDate: String? = null,
    val overview: Overview,
    val specialCategory: String? = null,
    val subtitle: Subtitle? = null,
    val title: Title
)

@Serializable
data class Overview(val en: List<En> = emptyList())

@Serializable
data class Subtitle(
    @SerialName("_type") val type: String,
    val en: String
)

@Serializable
data class Title(
    @SerialName("_type") val type: String? = null,
    val en: String
)

@Serializable
data class En(
    @SerialName("_key") val key: String? = null,
    @SerialName("_type") val type: String,
    val children: List<Children> = emptyList(),
    val markDefs: List<MarkDef> = emptyList(),
    val style: String
)

@Serializable
data class Children(
    @SerialName("_key") val key: String? = null,
    @SerialName("_type") val type: String,
    val marks: List<String> = emptyList(),
    val text: String
)

@Serializable
data class MarkDef(
    @SerialName("_key") val key: String,
    @SerialName("_type") val type: String,
    val blank: Boolean,
    val href: String? = null
)
