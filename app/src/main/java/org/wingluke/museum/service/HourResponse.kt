package org.wingluke.museum.service

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class HourResponse(
    val ms: Int,
    val query: String,
    @SerialName("result") val hourResult: HourResult
)

@Serializable
data class HourResult(
    val description: Description
)

@Serializable
data class Description(
    val en: String
)
