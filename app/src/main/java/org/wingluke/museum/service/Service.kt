package org.wingluke.museum.service

import retrofit2.http.GET
import retrofit2.http.Query

private const val QUERY_PATH = "data/query/production"
private const val HOURS_QUERY = "*[_type == 'hours'][0] {description{en}}"

interface Service {
    @GET(QUERY_PATH)
    suspend fun fetchRecent(@Query("query", encoded = true) query: String): ExhibitResponse

    @GET(QUERY_PATH)
    suspend fun fetchHours(@Query("query", encoded = true) query: String = HOURS_QUERY): HourResponse
}
