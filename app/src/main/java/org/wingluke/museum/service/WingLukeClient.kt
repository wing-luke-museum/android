package org.wingluke.museum.service

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import java.net.URL
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.wingluke.museum.room.Exhibit
import retrofit2.Retrofit
import toothpick.InjectConstructor

private const val API_DATE_FORMAT = "yyyy-MM-dd"
private const val EXHIBITS_URL = "https://1jr4bwbd.api.sanity.io/v2021-03-25/"

@InjectConstructor
class WingLukeClient {
    private val baseUrl = URL(EXHIBITS_URL)
    private val dateFormat = SimpleDateFormat(API_DATE_FORMAT, Locale.ENGLISH)
    private val contentType = "application/json".toMediaType()
    private val httpLoggingInterceptor = HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }
    private val okHttpClient = OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor).build()
    private val json = Json {
        isLenient = true
        ignoreUnknownKeys = true
    }

    @ExperimentalSerializationApi
    private val client: Service by lazy {
        Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(json.asConverterFactory(contentType))
            .client(okHttpClient)
            .build()
            .create(Service::class.java)
    }

    private val query =
        "*[_type == 'exhibit' && (closingDate > '${dateFormat.format(Date())}' || (closingDate == null && specialCategory == 'nowOnView') " +
                "|| specialCategory == 'alwaysOnView') && banner != null] | order(_createdAt asc) {\n" +
                "      _id,\n" +
                "      _type,\n" +
                "      title,\n" +
                "      subtitle,\n" +
                "      overview{en},\n" +
                "      openingDate,\n" +
                "      closingDate,\n" +
                "      specialCategory,\n" +
                "      'bannerURL': banner.asset->url,\n" +
                "    }"

    @ExperimentalSerializationApi
    suspend fun fetchRecent(): List<Exhibit> = client.fetchRecent(query).exhibitResult.map { exhibit ->
        with(exhibit) {
            Exhibit(
                id = id,
                title = title.en,
                startDate = openingDate?.parseDate,
                endDate = closingDate?.parseDate,
                duration = specialCategory.orEmpty(),
                description = overview.parse(),
                image = URL(bannerURL),
                subtitle = subtitle?.en ?: ""
            )
        }
    }

    private val String.parseDate get() = takeIf { isNotEmpty() }?.let(dateFormat::parse)

    @ExperimentalSerializationApi
    suspend fun fetchHours(): String = client.fetchHours().hourResult.description.en
}
