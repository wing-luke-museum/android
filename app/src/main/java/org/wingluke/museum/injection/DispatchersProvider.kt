package org.wingluke.museum.injection

import org.wingluke.museum.mvi.DispatcherProvider
import toothpick.InjectConstructor
import javax.inject.Provider

@InjectConstructor
class DispatchersProvider : Provider<DispatcherProvider> {
    override fun get(): DispatcherProvider = object : DispatcherProvider {}
}
