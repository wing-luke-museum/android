package org.wingluke.museum

import com.google.android.gms.maps.model.LatLng

object Constants {
    private const val museumLatitude = 47.5980975
    private const val museumLongitude = -122.3228245
    val museumLatLng = LatLng(museumLatitude, museumLongitude)
}
