package org.wingluke.museum.imdf

import com.google.android.gms.maps.model.LatLng
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

@Serializable(with = PolygonSerializer::class)
data class Polygon(val linearRings: List<LinearRing>) {
    @Transient val center: LatLng = linearRings.first().center
}

object PolygonSerializer : KSerializer<Polygon> {
    override val descriptor = ListSerializer(LinearRingSerializer).descriptor

    override fun serialize(encoder: Encoder, value: Polygon) {
        encoder.encodeSerializableValue(ListSerializer(LinearRingSerializer),
            value.linearRings)
    }

    override fun deserialize(decoder: Decoder): Polygon {
        val linearRings = decoder.decodeSerializableValue(ListSerializer(LinearRingSerializer))
        return Polygon(linearRings)
    }
}
