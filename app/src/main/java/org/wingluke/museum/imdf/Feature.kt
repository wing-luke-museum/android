package org.wingluke.museum.imdf

import kotlinx.serialization.Serializable

@Serializable
abstract class Feature
