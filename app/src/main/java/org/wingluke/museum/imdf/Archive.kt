package org.wingluke.museum.imdf

import android.content.res.AssetManager
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.io.BufferedReader
import java.io.InputStream

data class Archive(val assets: AssetManager, val subdirectory: String = "imdf") {
    val amenities = features<Amenity>(File.Amenity)
    val levels = features<Level>(File.Level)
    val units = features<Unit>(File.Unit)
    val venue = features<Venue>(File.Venue).first()

    //region JSON Parsing
    private inline fun <reified FeatureType : Feature> features(file: File): List<FeatureType> {
        val stream = stream(file)
        val jsonString = stream.bufferedReader().use(BufferedReader::readText)
        val featureCollection = Json { ignoreUnknownKeys = true }
            .decodeFromString<FeatureCollection<FeatureType>>(jsonString)
        return featureCollection.features
    }
    //endregion

    //region File Handling
    enum class File {
        Address, Amenity, Anchor, Building, Detail, Fixture, Footprint, Geofence, Kiosk,
        Level, Manifest, Occupant, Opening, Relationship, Section, Unit, Venue;

        val fileName: String get() = "${name.lowercase()}.geojson"
    }

    private fun stream(file: File): InputStream {
        val filePath = "$subdirectory/${file.fileName}"
        return assets.open(filePath)
    }
    //endregion
}
