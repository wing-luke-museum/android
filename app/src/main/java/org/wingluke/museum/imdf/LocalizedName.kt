package org.wingluke.museum.imdf

import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.builtins.MapSerializer
import kotlinx.serialization.builtins.serializer
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

@Serializable(with = LocalizedNameSerializer::class)
data class LocalizedName(val localizations: Map<String, String>) {
    var localized: String = localizations.getValue("en")
}

object LocalizedNameSerializer : KSerializer<LocalizedName> {
    private val serializer = MapSerializer(String.serializer(), String.serializer())
    override val descriptor = serializer.descriptor

    override fun serialize(encoder: Encoder, value: LocalizedName) {
        encoder.encodeSerializableValue(serializer, value.localizations)
    }

    override fun deserialize(decoder: Decoder): LocalizedName {
        val localizations = decoder.decodeSerializableValue(serializer)
        return LocalizedName(localizations)
    }
}
