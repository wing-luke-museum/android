package org.wingluke.museum.imdf

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Unit(
    val properties: Properties,
    val geometry: PolygonGeometry,
    @SerialName("id") val identifier: String
) : Feature() {
    @Serializable
    data class Properties(
        val category: Category,
        @SerialName("level_id") val levelID: String,
        val name: LocalizedName?,
        val space: String? = null
    )

    val name: String? = properties.name?.localized

    val isRestroom: Boolean
        get() = when (properties.category) {
            Category.FamilyRestroom,
            Category.FemaleRestroom,
            Category.FemaleWheelchairRestroom,
            Category.MaleRestroom,
            Category.MaleWheelchairRestroom,
            Category.TransgenderRestroom,
            Category.TransgenderWheelchairRestroom,
            Category.UnisexRestroom,
            Category.UnisexWheelchairRestroom,
            Category.WheelchairRestroom -> true
            else -> false
        }

    val category: Unit.Category
        get() = properties.category

    val space: String?
        get() = properties.space

    @Serializable
    enum class Category {
        @SerialName("auditorium") Auditorium,
        @SerialName("brick") Brick,
        @SerialName("classroom") Classroom,
        @SerialName("column") Column,
        @SerialName("concrete") Concrete,
        @SerialName("conferenceroom") ConferenceRoom,
        @SerialName("drywall") Drywall,
        @SerialName("elevator") Elevator,
        @SerialName("escalator") Escalator,
        @SerialName("fieldofplay") FieldOfPlay,
        @SerialName("firstaid") FirstAid,
        @SerialName("fitnessroom") FitnessRoom,
        @SerialName("foodservice") Foodservice,
        @SerialName("footbridge") Footbridge,
        @SerialName("glass") Glass,
        @SerialName("huddleroom") HuddleRoom,
        @SerialName("kitchen") Kitchen,
        @SerialName("laboratory") Laboratory,
        @SerialName("library") Library,
        @SerialName("lobby") Lobby,
        @SerialName("lounge") Lounge,
        @SerialName("mailroom") MailRoom,
        @SerialName("mothersroom") MothersRoom,
        @SerialName("movietheater") MovieTheater,
        @SerialName("movingwalkway") MovingWalkway,
        @SerialName("nonpublic") Nonpublic,
        @SerialName("office") Office,
        @SerialName("opentobelow") OpenToBelow,
        @SerialName("parking") Parking,
        @SerialName("phoneroom") PhoneRoom,
        @SerialName("platform") Platform,
        @SerialName("privatelounge") PrivateLounge,
        @SerialName("ramp") Ramp,
        @SerialName("recreation") Recreation,
        @SerialName("restroom") Restroom,
        @SerialName("restroom.family") FamilyRestroom,
        @SerialName("restroom.female") FemaleRestroom,
        @SerialName("restroom.female.wheelchair") FemaleWheelchairRestroom,
        @SerialName("restroom.male") MaleRestroom,
        @SerialName("restroom.male.wheelchair") MaleWheelchairRestroom,
        @SerialName("restroom.transgender") TransgenderRestroom,
        @SerialName("restroom.transgender.wheelchair") TransgenderWheelchairRestroom,
        @SerialName("restroom.unisex") UnisexRestroom,
        @SerialName("restroom.unisex.wheelchair") UnisexWheelchairRestroom,
        @SerialName("restroom.wheelchair") WheelchairRestroom,
        @SerialName("road") Road,
        @SerialName("room") Room,
        @SerialName("serverroom") Serverroom,
        @SerialName("shower") Shower,
        @SerialName("smokingarea") Smokingarea,
        @SerialName("stairs") Stairs,
        @SerialName("steps") Steps,
        @SerialName("storage") Storage,
        @SerialName("structure") Structure,
        @SerialName("terrace") Terrace,
        @SerialName("theater") Theater,
        @SerialName("unenclosedarea") Unenclosedarea,
        @SerialName("unspecified") Unspecified,
        @SerialName("vegetation") Vegetation,
        @SerialName("waitingroom") Waitingroom,
        @SerialName("walkway") Walkway,
        @SerialName("walkway.island") WalkwayIsland,
        @SerialName("wood") Wood
    }
}
