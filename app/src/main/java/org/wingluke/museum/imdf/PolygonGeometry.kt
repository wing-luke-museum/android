package org.wingluke.museum.imdf

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
data class PolygonGeometry(@SerialName("coordinates") val polygon: Polygon) {
    @Transient val center = polygon.center
}
