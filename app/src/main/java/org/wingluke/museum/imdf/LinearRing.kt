package org.wingluke.museum.imdf

import com.google.android.gms.maps.model.LatLng
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

@Serializable(with = LinearRingSerializer::class)
data class LinearRing(val coordinates: List<Coordinate>) {
    @Transient val center: LatLng
    get() {
        val minLat = coordinates.fold(coordinates.first(), { latest, next ->
            if (latest.latitude <= next.latitude) {
                latest
            } else {
                next
            }
        }).latitude

        val maxLat = coordinates.fold(coordinates.first(), { latest, next ->
            if (latest.latitude >= next.latitude) {
                latest
            } else {
                next
            }
        }).latitude

        val minLong = coordinates.fold(coordinates.first(), { latest, next ->
            if (latest.longitude <= next.longitude) {
                latest
            } else {
                next
            }
        }).longitude

        val maxLong = coordinates.fold(coordinates.first(), { latest, next ->
            if (latest.longitude >= next.longitude) {
                latest
            } else {
                next
            }
        }).longitude

        val midLat = (minLat + maxLat) / 2.0
        val midLong = (minLong + maxLong) / 2.0
        return LatLng(midLat, midLong)
    }
}

object LinearRingSerializer : KSerializer<LinearRing> {
    override val descriptor = ListSerializer(CoordinateSerializer).descriptor

    override fun serialize(encoder: Encoder, value: LinearRing) {
        encoder.encodeSerializableValue(ListSerializer(CoordinateSerializer),
            value.coordinates)
    }

    override fun deserialize(decoder: Decoder): LinearRing {
        val coordinates = decoder.decodeSerializableValue(ListSerializer(CoordinateSerializer))
        return LinearRing(coordinates)
    }
}
