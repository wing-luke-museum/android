package org.wingluke.museum.imdf

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Venue(
    val properties: Properties,
    val geometry: PolygonGeometry
) : Feature() {
    @Serializable
    data class Properties(
        @SerialName("address_id") val addressID: String
    )

    var polygon = geometry.polygon
}
