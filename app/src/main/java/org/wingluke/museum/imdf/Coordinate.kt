package org.wingluke.museum.imdf

import com.google.android.gms.maps.model.LatLng
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.builtins.DoubleArraySerializer
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

@Serializable(with = CoordinateSerializer::class)
data class Coordinate(val latitude: Double, val longitude: Double) {
    fun toLatLng(): LatLng {
        return LatLng(latitude, longitude)
    }
}

object CoordinateSerializer : KSerializer<Coordinate> {
    override val descriptor = DoubleArraySerializer().descriptor

    override fun serialize(encoder: Encoder, value: Coordinate) {
        val doubles = doubleArrayOf(value.latitude, value.longitude)
        encoder.encodeSerializableValue(DoubleArraySerializer(), doubles)
    }

    override fun deserialize(decoder: Decoder): Coordinate {
        val doubles = decoder.decodeSerializableValue(DoubleArraySerializer())
        return Coordinate(doubles[1], doubles[0])
    }
}
