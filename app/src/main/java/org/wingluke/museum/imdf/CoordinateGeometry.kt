package org.wingluke.museum.imdf

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class CoordinateGeometry(@SerialName("coordinates") val coordinate: Coordinate) {
    fun toLatLng() = coordinate.toLatLng()
}
