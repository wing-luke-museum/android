package org.wingluke.museum.imdf

import kotlinx.serialization.Serializable

@Serializable
data class FeatureCollection<FeatureType : Feature>(val features: List<FeatureType>)
