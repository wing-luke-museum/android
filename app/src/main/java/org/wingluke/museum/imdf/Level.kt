package org.wingluke.museum.imdf

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Level(
    @SerialName("id") val identifier: String,
    private val properties: Properties
) : Feature() {
    val ordinal = properties.ordinal
    val shortName = properties.shortName.localized

    @Serializable
    data class Properties(
        val name: LocalizedName,
        val ordinal: Int,
        @SerialName("short_name") val shortName: LocalizedName
    )
}
