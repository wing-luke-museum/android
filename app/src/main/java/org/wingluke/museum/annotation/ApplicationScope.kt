package org.wingluke.museum.annotation

import javax.inject.Scope

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Scope
annotation class ApplicationScope
