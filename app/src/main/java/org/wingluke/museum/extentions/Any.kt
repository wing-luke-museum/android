package org.wingluke.museum.extentions

val Any?.isNull: Boolean get() = this == null

val Any?.isNotNull: Boolean get() = !this.isNull
