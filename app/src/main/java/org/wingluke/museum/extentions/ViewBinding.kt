package org.wingluke.museum.extentions

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.viewbinding.ViewBinding
import java.lang.IllegalStateException

fun <V : ViewBinding> Fragment.viewBinding(factory: (LayoutInflater, ViewGroup?, Boolean) -> V) =
    FragmentViewBinding(this, factory)

class FragmentViewBinding<V : ViewBinding> : LifecycleObserver {

    private val fragment: Fragment
    private val bindFactory: ((View) -> V)?
    private val inflateFactory: ((LayoutInflater, ViewGroup?, Boolean) -> V)?
    private val fragmentName get() = fragment.javaClass.name

    constructor(fragment: Fragment, factory: (LayoutInflater, ViewGroup?, Boolean) -> V) {
        this.fragment = fragment
        inflateFactory = factory
        bindFactory = null
    }

    constructor(fragment: Fragment, factory: (View) -> V) {
        this.fragment = fragment
        bindFactory = factory
        inflateFactory = null
    }

    val root get() = view.root

    val view: V
        get() {
            check(isInitialized) { "($fragmentName) Should not attempt to get bindings after views are destroyed" }
            return checkNotNull(binding) { "($fragmentName) Missing call to $factoryMethod" }
        }

    val viewOrNull get() = binding?.takeIf { isInitialized }

    private val isInitialized get() = lifecycle.currentState.isAtLeast(Lifecycle.State.INITIALIZED)

    private val lifecycle
        get() = runCatching { fragment.viewLifecycleOwner.lifecycle }
            .recover { throw IllegalStateException("($fragmentName) View lifecycle is null", it) }
            .getOrThrow()

    private val factoryMethod get() = "FragmentViewBinding.${if (inflateFactory != null) "inflate" else "bind"}()"

    private var binding: V? = null

    fun bind(view: View): V {
        checkNotNull(bindFactory) { "($fragmentName) FragmentViewBinding was not created with a bind method" }
        return create { bindFactory.invoke(view) }
    }

    fun inflate(inflater: LayoutInflater, container: ViewGroup?, attachToRoot: Boolean = false): V {
        checkNotNull(inflateFactory) { "($fragmentName) FragmentViewBinding was not created with an inflate method" }
        return create { inflateFactory.invoke(inflater, container, attachToRoot) }
    }

    private inline fun create(factory: () -> V): V {
        check(binding == null) { "($fragmentName) $factoryMethod has already been called" }
        lifecycle.addObserver(this)
        return factory().also { binding = it }
    }

    @Suppress("UnusedPrivateMember")
    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private fun onDestroyView() {
        binding = null
    }
}
