package org.wingluke.museum

import android.app.Application
import androidx.room.Room
import com.github.stephanenicolas.toothpick.smoothie.BuildConfig
import org.wingluke.museum.annotation.ApplicationScope
import org.wingluke.museum.injection.DispatchersProvider
import org.wingluke.museum.mvi.DispatcherProvider
import org.wingluke.museum.room.AppDatabase
import toothpick.Scope
import toothpick.Toothpick
import toothpick.configuration.Configuration
import toothpick.ktp.KTP
import toothpick.ktp.binding.bind
import toothpick.ktp.binding.module
import toothpick.smoothie.module.SmoothieApplicationModule

class WingLukeApplication : Application() {
    @Suppress("LateinitUsage")
    lateinit var scope: Scope

    private val appDatabase by lazy {
        Room.databaseBuilder(applicationContext, AppDatabase::class.java, "wing-luke").build()
    }

    override fun onCreate() {
        super.onCreate()

        Toothpick.setConfiguration(
            if (BuildConfig.DEBUG) Configuration.forDevelopment() else Configuration.forProduction()
        )

        scope = KTP.openScope(ApplicationScope::class.java).installModules(
            SmoothieApplicationModule(this),
            module {
                bind<DispatcherProvider>().toProvider(DispatchersProvider::class)
                bind<AppDatabase>().toInstance(appDatabase)
            }
        )
    }

    override fun onTrimMemory(level: Int) {
        super.onTrimMemory(level)
        scope.release()
    }
}
