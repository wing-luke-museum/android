# android

## Maps APi

Add `MAPS_API_KEY=YOUR_API_KEY` to local.properties file

## App Link Test

### Exhibits


`adb shell pm get-app-links org.wingluke.museum`

```
adb shell am start -a android.intent.action.VIEW \
    -c android.intent.category.BROWSABLE \
    -d "https://winglukemuseum.github.io"
```
